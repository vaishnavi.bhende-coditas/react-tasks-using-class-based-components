import { Component, ReactNode } from "react";
import styles from "./App.module.scss";
import Dashboard from "./components/Dashboard/Dashboard";

class App extends Component {
  render(): ReactNode {
    return (
      <Dashboard />
    )
  }
}

export default App;