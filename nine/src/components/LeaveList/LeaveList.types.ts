import { LeaveProps } from "../Leave/Leave.types";

export interface LeaveListProps { 
  title: string
  leaves: LeaveProps[];
  action: ((props:any) => JSX.Element)[];
  handleClick: (id: number, type: string) => {};
}