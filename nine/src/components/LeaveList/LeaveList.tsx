import { Component, ReactNode } from "react";
import { LeaveListProps } from "./LeaveList.types";
import styles from "./LeaveList.module.scss"
import Leave from "../Leave/Leave";

class LeaveList extends Component<LeaveListProps> {
  render(): ReactNode {
    return (
      <>
        <div className={styles.LeaveList}>
          <h2>{this.props.title}</h2>
          {
            this.props.leaves.map((leave: any, key: any) => {
              console.log(leave)
                  return(
                      <Leave
                      key={key}
                      id={leave.id}
                      date={leave.date}
                      description={leave.description}
                      action={this.props.action}
                      handleClick={this.props.handleClick}
                      />
                  );
            })
          }
        </div>
      </>
    )
  }
}

export default LeaveList;