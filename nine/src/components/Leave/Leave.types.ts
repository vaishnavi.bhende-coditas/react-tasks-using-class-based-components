
export interface LeaveProps {
  id:number,
  date: string;
  description: string;
  // action: (id: number, date: string, description: string) => JSX.Element;
  action: ((props: any) => JSX.Element)[];
  handleClick: (id: number, type:string) => void;
}