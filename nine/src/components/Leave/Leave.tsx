import { Component, ReactNode } from "react";
import styles from "./Leave.module.scss";
import { LeaveProps } from "./Leave.types";


class Leave extends Component<LeaveProps> {
  [x: string]: number;

  constructor(props: LeaveProps) {
    super(props);
    
  }

  render(): ReactNode {

    
    return (
      <>
        <div className={styles.Leave}>
          <div className={styles.LeaveInfo}>
            <span>{this.props.date}</span>
            <span>{this.props.description}</span>
          </div>

          {this.props.action.map((Btn, key) => (
          <Btn
            key={key}
            onClick={(e: MouseEvent) =>
              this.props.handleClick(this.props.id, (e.target as HTMLButtonElement).innerText)
            }
          />
        ))}
        </div>
      </>
    )
  }
}

export default Leave;