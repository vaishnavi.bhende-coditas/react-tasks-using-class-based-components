import { ButtonHTMLAttributes, Component, ReactNode, HTMLAttributes, useState } from "react";
import styles from "./Dashboard.module.scss";
import LeaveList from "../LeaveList/LeaveList";
import { leaves } from "../../leave.data";
import { LeaveListProps } from "../LeaveList/LeaveList.types";


class Dashboard extends Component {
  render(): ReactNode {
    const [data, setNewData] = useState(leaves);

    const createBtn = (text: "M" | "P" | "S") => {
      return (props: ButtonHTMLAttributes<HTMLButtonElement>) => {
        return (
          <button {...props} className={styles[text]}>
            {text}
          </button>
        );
      };
    };

    const handleClick = (id: number, type: string) => {
      const updatedLeaves = data.map((leaveItem) => {
        if (leaveItem.id === id) {
          return { ...leaveItem, type: type as "M" | "P" | "S" };
        } else {
          return leaveItem;
        }
      });
      setNewData(updatedLeaves);
    };

    const mandatoryBtn = createBtn("M");
    const sickBtn = createBtn("S");
    const plannedBtn = createBtn("P");

    return (
      <>
        <div className={styles.Dashboard}>
          <div className={styles.Heading}>
            <h1>My Leaves</h1>
          </div>

          <div className={styles.LeaveListBox}>

            <LeaveList
              title="Mandatory"
              leaves={data.filter((l) => l.type === "M")}
              action={[sickBtn, plannedBtn]}
              handleClick={handleClick}
            />
            <LeaveList
              title="Planned"
              leaves={data.filter((l) => l.type === "P")}
              action={[sickBtn, mandatoryBtn]}
              handleClick={handleClick}
            />
            <LeaveList
              title="Sick"
              leaves={data.filter((l) => l.type === "S")}
              action={[mandatoryBtn, plannedBtn]}
              handleClick={handleClick}
            />
          </div>
        </div>
      </>
    );
  }
}

export default Dashboard;
