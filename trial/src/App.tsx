import { Component } from "react";


class App extends Component<{}, { count: number }> {

  constructor(props: {}) {
    super(props);

    this.state = {
      count: 0
    }
  }

  handleIncrement() {
    console.log(this.state.count);
    this.setState({ count: this.state.count + 1 });
  }

  render() {
    return (
      <button onClick={() => this.handleIncrement}>{this.state.count}</button>
    )
  }
}

export default App;