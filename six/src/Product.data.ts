
export const ProductArray = [
  {
    ProductId: 1,
    Image: "../src/assets/image/product1.jpg",
    Name: "Makeup",
    Price: 999,
    ButtonText: "Be beautiful"
  },
  {
    ProductId: 2,
    Image: "../src/assets/image/butterfly-dress.webp",
    Name: "Makeup",
    Price: 999,
    ButtonText: "Be beautiful"
  },
  {
    ProductId: 3,
    Image: "../src/assets/image/dinnerset.webp",
    Name: "Makeup",
    Price: 999,
    ButtonText: "Be beautiful"
  }
]