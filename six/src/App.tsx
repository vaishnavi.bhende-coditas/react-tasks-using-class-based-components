import { Component, ReactNode } from "react"
import styles from "./App.module.scss"

import ProductList from "./components/ProductList/ProductList"
import { ProductArray } from "./Product.data"

class App extends Component {
  render(): ReactNode {
    return (
      <>
        <div className={styles.ProductsArr}>
      <ProductList products={ProductArray}/>
      </div>
      </>
    )
  }
}