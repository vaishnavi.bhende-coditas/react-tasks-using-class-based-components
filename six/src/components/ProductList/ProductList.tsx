import { Component, ReactNode } from 'react';
import Product from "../Product/Product"
import { ProductProps } from '../Product/Product.types';

class ProductList extends Component<{products: any}> {
  render(): ReactNode {
    const EachProduct = products.map((product: ProductProps) => {
      return(
        <Product 
          ProductId={product.ProductId}
          Name={product.Name}
          Price={product.Price}
          Image={product.Image}
          ButtonText={product.ButtonText}
        />
      )
    })
    return (
      <>
        {EachProduct}
      </>
    )
  }
}

export default ProductList;