
export interface ProductProps {
  ProductId: number,
  Name: string,
  Price: number,
  Image: string,
  ButtonText: string
}