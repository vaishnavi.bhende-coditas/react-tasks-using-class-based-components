import { Component, ReactNode } from 'react';
import styles from './Product.module.scss'
import { ProductProps } from './Product.types';

class Product extends Component<{Image: string; Name: string; Price: number; ButtonText: string}, {}> {
  render(): ReactNode {
    return (
      <div className={styles.ProductContainer}>
        <div className={styles.ImageContainer}>
          <img src={this.props.Image} alt={this.props.Name} />
        </div>

        <div className={styles.ProductDetails}>
          <h2>Product Name: {this.props.Name}</h2>
          <h3>Product Price: {this.props.Price}</h3>
          <button className={styles.Btn}>{this.props.ButtonText}</button>
        </div>
      </div>
 
    )
  }
}

export default Product;