import { Component, ReactNode } from "react";

//Class Component
class AppComponent extends Component {
  render(): ReactNode {
    return (
      <div>
        <p>This is a class Component</p>
      </div>
    );
  }
}

export default AppComponent;
