import { Component, ReactNode, useState } from 'react';
import styles from './Counter.module.scss'
import { CounterProps } from './Counter.types';


class Counter extends Component <{},{count: number}> {

  constructor(props:{}){
    super(props);
    this.state={
      count:0
    }
  }
  render(): ReactNode {

   

  const increment = () => {
    this.setState({
      count: this.state.count+1
    })
  }

  const decrement = () => {
    this.setState({
      count: this.state.count-1
    })
  }

    return(
      <div className={styles.Counter}>
      <span onClick={decrement}>-</span>
      <span>{this.state.count}</span>
      <span onClick={increment}>+</span>
    </div>
    )
  }
  
}

export default Counter;