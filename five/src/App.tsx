import Counter from "./components/Counter";
import "./App.css";
import { Component, ReactNode } from "react";

class App extends Component {
  render(): ReactNode {
    return (
      <>
        <Counter />
      </>
    );
  }
}

export default App;
