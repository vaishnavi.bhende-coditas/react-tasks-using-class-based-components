// import { Component, ReactNode } from "react"
// import Friends from "./components/Friend"
import Profile from "./components/Profile";
import main from "./components/Server"

const {Server, ServerClass} = main;

const App = () => {
  return (
    <>
    {/* <Friends.Friend />
    <Friends.FriendClass /> */}
    <Server 
      name= "abc"
      status= "on"
    />
    <ServerClass 
      name= "server1"
      status= {true}
    />

    <Profile
      name="vaishnavi"
      surname= "bhende"
      age= {22}
      isMarried= {false}
      friends= {['a', 'b', 'c']}
      address= {{'city':'Pune', 'country': 'India'}}
    />
    </>
  )
}


export default App
