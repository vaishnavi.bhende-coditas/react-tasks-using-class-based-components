import {Component, ReactNode} from 'react'

const Server = (props:any) => {
    return (
      <>
        <div >
          <p>{props.name}</p>
          <p>{props.status ? <button>stop server</button> :<button>start server</button> }</p>
          
        </div>
      </>
    )
  }

  class ServerClass extends Component<any> {                           // {name: string, status: string}
    constructor (props:any){
        super(props);
    }
    render(): ReactNode {
      return (
        <>
        <div>
          <p>{this.props.name}</p>
          <p>{this.props.status}</p>
        </div>
      </>
      )
    }
  }
  

export default {
    Server,
    ServerClass
}