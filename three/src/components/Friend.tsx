import { Component, ReactNode } from "react"

const Friend = (parameter: any) => {
  console.log(parameter);
  return (
    <>
      <span>Vaishnavi</span>
    </>
  )
}

class FriendClass extends Component {
  render(): ReactNode {
    return (
      <>
      <span>Bhende</span>
    </>
    )
  }
}

export default {
  Friend,
  FriendClass
}