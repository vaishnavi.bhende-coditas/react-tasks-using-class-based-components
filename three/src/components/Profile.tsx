

const Profile = (props: any) => {
  return (
    <>
      <p>{props.name}</p>
      <p>{props.surname}</p>
      <p>{props.age}</p>
      <p>{props.isMarried ? 'yes' : 'no'}</p>
      {/* <p>{props.friends}</p> */}
      {
        props.friends.map((friend: any) => {
          return <p>{friend}</p>
        })
      }

      <p>{props.address.city}, {props.address.country}</p>
    </>
  )
}

export default Profile;