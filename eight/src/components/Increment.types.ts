
export interface incrementprops {
  count: number;
  increment: () => void;
}