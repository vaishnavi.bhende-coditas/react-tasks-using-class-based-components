import { Component, ReactNode } from 'react';
import styles from './Increment.module.scss';

class Increment extends Component<{}, { count: number }> {

  constructor(props: {}) {
    super(props);

  }

  // const increment(() => {

  // })

  render(): ReactNode {
    
    return (
      <>
      <div className={styles.Container}>
        <span >{this.state.count}</span>
        <button onClick={increment}>Increment</button>
      </div>
    </>
    )
  }
}

export default Increment;