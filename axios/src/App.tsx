import style from "./App.module.scss";
import Pokemon from "./components/Pokemon/Pokemon";

const App = () => {
  return (
    <>
      <Pokemon />
    </>
  )
}
export default App;