import PokemonList from "../PokemonList/PokemonList";
import style from "./Pokemon.module.scss";

const Pokemon = () => {

  return (
    <div>
      <h1>Pokemon List</h1>
      <div className={style.mainContainer}>
        <div className={style.allPokemon}>
          <h3>All Pokemons</h3>
          <PokemonList />
        </div>
        <div className={style.caughtPokemon}>
          <h3>Caught Pokemons</h3>
        </div>
      </div>
    </div>
  )
}
export default Pokemon;