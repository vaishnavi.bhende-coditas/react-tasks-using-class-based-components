import styles from './Dot.module.scss'
import '../../App.types'
import { Component } from 'react'


const Dot = () => {
    return (
        <div></div>
    )
}

export const DotOn = () => { return <div className={styles.DotOnline}><Dot/></div> }

export const DotOff = () => { return <div className={styles.DotOffline}><Dot/></div> }

class DotOnComponent extends Component{
    render(){
        return(
            <div className={styles.DotOnline}><Dot/></div>
        )
    }
}
class DotOffComponent extends Component{
    render(){
        return(
            <div className={styles.DotOffline}><Dot/></div>
        )
    }
}

export default{DotOffComponent, DotOnComponent}




































// const Dot = ({isActive} : DotStatus) => {
//     const DotClass = isActive ? <DotOn/> : <DotOff/>;
//     return (
//         <>
//         {DotClass}
//         </>
//     )
// }

// export default Dot;