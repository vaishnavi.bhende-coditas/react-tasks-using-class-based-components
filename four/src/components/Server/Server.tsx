import styles from "./Server.module.scss";
import { DotOff, DotOn } from "../Dot/Dot";
import { PillOn, PillOff } from "../Pill/Pill";
import classPillComponents from "../Pill/Pill"
import classDotComponents from "../Dot/Dot"

const {PillOffComponent, PillOnComponent} = classPillComponents;
const {DotOffComponent, DotOnComponent} = classDotComponents;

import { Component } from "react";
const Server = (props: propServer) => {
  let pillToRender = props.isActive ? <PillOn/> : <PillOff/>
  let dotToRender = props.isActive ? <DotOn/> : <DotOff/>
  let Status : boolean = props.isActive;
  return (
    <div className={styles.Server}>
      <div>
        <h2>{props.name}</h2>
      </div>
 
      <div>
       {pillToRender}
        {dotToRender}
      </div>
      <button className={styles.buttonContainer}>{Status ? "Stop Server" : "Start Server"}</button>
    </div>
  );
};

interface ServerProps{
  name: string,
  isActive: boolean
}
class ServerComponent extends Component<{name: string, isActive: boolean},{}>{
  
  constructor(props: ServerProps){
    super(props);
  }
 

  render(){
    let pillToRender = this.props.isActive ? <PillOnComponent/> : <PillOffComponent/>
    let dotToRender = this.props.isActive ? <DotOnComponent/> : <DotOffComponent/>
    let Status : boolean = this.props.isActive;
    return(
      <>
      <div className={styles.Server}>
      <div>
        <h2>{this.props.name}</h2>
      </div>
 
      <div>
       {pillToRender}
        {dotToRender}
      </div>
      <button className={styles.buttonContainer}>{Status ? "Stop Server" : "Start Server"}</button>
    </div>
      </>
    )
  }
}
export default {Server, ServerComponent};
