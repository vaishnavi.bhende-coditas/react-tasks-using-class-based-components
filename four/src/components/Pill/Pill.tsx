import styles from './Pill.module.scss'
import '../../App.types'
import { Component } from 'react';

const Pill = () => {
    
}

// PillOn component
export const PillOn = () => {
    return <div className={styles.PillOnline}>Online</div>;
};

// PillOff component
export const PillOff = () => {
    return <div className={styles.PillOffline}>Offline</div>;
};

class PillOnComponent extends Component{
    render(){
        return(
            <div className={styles.PillOnline}>Online</div>
        )
    }
}
class PillOffComponent extends Component{
    render(){
        return(
            <div className={styles.PillOffline}>Offline</div>
        )
    }
}

export default{PillOffComponent, PillOnComponent}




























// const Pill = ({isActive}:PillStatus) => {
//     const PillClass = isActive ? <PillOn/> : <PillOff/>;
//     // const PillClass = isActive ? styles.PillOnline : styles.PillOffline;
//     // const text = isActive ? "Online" : "Offline"
//     return(
//         <>
//            {PillClass}
//         </>
//     )
// }


// export default Pill;