import Servers from "./components/Server/Server";
import styles from "./App.module.scss";
import { Component } from "react";
const {Server, ServerComponent} = Servers
const App = () => {
  const servers = [
    {name:"MC2-L4", isActive:true},
    {name:"MC2-M4", isActive:false},
    {name:"MC2-R4", isActive:true},
    {name:"MC2-T3", isActive:false}
  ]
  return (
    <>
      <div className={styles.ServerContainer}>
       <div>
       {servers.map((server)=>(
          <ServerComponent name = {server.name} isActive = {server.isActive}/>
        ))}
       </div>
       <div>
       {servers.map((server)=>(
          <Server name = {server.name} isActive = {server.isActive}/>
        ))}
       </div>
      </div>
    </>
  );
};

class AppComponent extends Component{
  render() {
    const servers = [
      {name:"MC2-L4", isActive:true},
      {name:"MC2-M4", isActive:false},
      {name:"MC2-R4", isActive:true},
      {name:"MC2-T3", isActive:false}
    ]
    return(
      <>
        <div className={styles.ServerContainer}>
       <div>
       {servers.map((server)=>(
          <ServerComponent name = {server.name} isActive = {server.isActive}/>
        ))}
       </div>
       <div>
       {servers.map((server)=>(
          <Server name = {server.name} isActive = {server.isActive}/>
        ))}
       </div>
      </div>
      </>
    )
  }
}

export default {App, AppComponent};
