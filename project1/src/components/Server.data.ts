import { ServerType } from "./Server.types"

export const servers: ServerType[] = [
  {serverName: 'EC2-L1', isActive: true},
  {serverName: 'EC2-M4', isActive: false},
  {serverName: 'EC2-T3', isActive: false},
]