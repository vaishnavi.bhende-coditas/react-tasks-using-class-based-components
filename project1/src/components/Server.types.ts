export interface ServerType {
  serverName: string,
  isActive: boolean
}