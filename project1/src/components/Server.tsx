import { Component, ReactNode } from "react";
import styles from "./Server.module.scss";
import { ServerType } from "./Server.types";

class Server extends Component<{serverName: string; isActive: boolean}, {}> {

  constructor(props: ServerType) {
    super(props);
  }
  
  render(): ReactNode {

    // const { serverName, isActive } = this.props;

    // const { serverName } = this.props;
    // const { isActive } = this.state;

    return (
      <>
        <div className={styles.Server}>
        <h2 className={styles.ServerName}>{this.props.serverName}</h2>
        <div>
          <p
            className={`${styles.Pill} ${
              this.props.isActive ? styles.PillOn : styles.PillOff
            }`}
          >
            {this.props.isActive ? "Online" : "Offline"}
          </p>
          <span
            className={`${styles.Dot} ${
              this.props.isActive ? styles.DotOn : styles.DotOff
            }`}
          ></span>
        </div>
        <button className={styles.Btn}>
          {this.props.isActive ? "Stop Server" : "Start Server"}
        </button>
      </div>
      </>
    )
  }
}

export default Server;