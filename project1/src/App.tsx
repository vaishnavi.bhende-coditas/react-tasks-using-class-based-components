import { Component, ReactNode } from "react";
import Server from "./components/Server";
import { servers } from "./components/Server.data"

class App extends Component {
  render(): ReactNode {
    return(
      <div>
        
        {servers.map((server) => {
          return <Server serverName={server.serverName} isActive={server.isActive} />;
        })}
      </div>
    )
  }
}

export default App;