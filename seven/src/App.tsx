import { Component, ReactNode } from "react";
import Counter from "./components/Counter";


class App extends Component {
  render(): ReactNode {
    return (
      <>
        <Counter />
      </>
    )
  }
}

export default App;