import { Component, ReactNode, useState } from 'react';
import styles from './Counter.module.scss';

class Counter extends Component {
  render(): ReactNode {

    const [count1, setCount1] = useState(0);
  const [count2, setCount2] = useState(0);

  const increment1 = () => {
    setCount1(count1+1);
  }

  const increment2 = () => {
    setCount2(count2+1);
  }

    return (
      <>
      <div className={styles.CounterContainer}>

        <div className={styles.Container1}>
          <span>{count2}</span>
          <button onClick={increment1}>Increment</button>
        </div>

        <div className={styles.Container2}>
          <span>{count1}</span>
          <button onClick={increment2}>Increment</button>
        </div>

      </div>
    </>
    )
  }
}

export default Counter;